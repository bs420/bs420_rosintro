# based on tutorial from:
#https://github.com/ros-planning/moveit_tutorials/blob/master/
#doc/move_group_python_interface/scripts/move_group_python_interface_tutorial.py

#!/usr/bin/env python

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, SRI International
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of SRI International nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Acorn Pooley, Mike Lautman

## BEGIN_SUB_TUTORIAL imports
##
## To use the Python MoveIt interfaces, we will import the `moveit_commander`_ namespace.
## This namespace provides us with a `MoveGroupCommander`_ class, a `PlanningSceneInterface`_ class,
## and a `RobotCommander`_ class. More on these below. We also import `rospy`_ and some messages that we will use:

from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list


def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True

class write_initials(object):
    """Write Initials"""

    def __init__(self):
        super(write_initials, self).__init__()

        # First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        # Instantiate a `RobotCommander`_ object. 
        robot = moveit_commander.RobotCommander()

        # Instantiate a `PlanningSceneInterface`_ object.
        scene = moveit_commander.PlanningSceneInterface()

        # Instantiate a `MoveGroupCommander`_object to interface with planning group
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        # Create a `DisplayTrajectory`_ ROS publisher which is used to display
        # trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        #getting reference frame of robot
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # Print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # Get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Print the entire state of the robot for debugging:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        # Misc variables
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self, joint0, joint1, joint2, joint3, joint4, joint5):
        #move robot arm based on inputted joint values
        move_group = self.move_group

        # establishing joint values from function inputs
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = joint0
        joint_goal[1] = joint1
        joint_goal[2] = joint2
        joint_goal[3] = joint3
        joint_goal[4] = joint4
        joint_goal[5] = joint5

        # move arm to joint value inputs
        move_group.go(joint_goal, wait=True)
        move_group.stop() #call "stop" to ensure no residual movement

        # For testing joint value move:
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def plan_cartesian_path(self, scale, path_points):
        #plan path using inputted list of points
        move_group = self.move_group

        waypoints = [] #establishing list of waypoints for planning
        wpose = move_group.get_current_pose().pose
        
        #iterate through list inputted in function
        for i in range(len(path_points)):
            wpose.position.x += scale * path_points[i][0] #change along x
            wpose.position.y += scale * path_points[i][1] #change along y
            wpose.position.z += scale * path_points[i][2] #change along z
            waypoints.append(copy.deepcopy(wpose))
       

        # Cartesian path to be interpolated at a resolution of 0.01 m
        # Disable the jump threshold by setting it to 0.0
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        return plan, fraction

    def execute_plan(self, plan):
        #execute plan from plan_cartesian_path
        move_group = self.move_group
        move_group.execute(plan, wait=True)

def main():
    try:
        print("")
        print("----------------------------------------------------------")
        print("Writing my initial. Press Ctrl-D to exit at any time")
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit at any time")
        print("")
        input(
            "============ Press `Enter` to set up the moveit_commander ..."
        )
        tutorial = write_initials()

        input(
           "= Press `Enter` to move to starting point..."
        )
        tutorial.go_to_joint_state(20*tau/360, -78*tau/360, 51*tau/360, -30*tau/360, 10*tau/360, 200*tau/360)

        input("============ Press `Enter` to write the letter 'B' with a Cartesian path ...")
        cartesian_plan, fraction = tutorial.plan_cartesian_path(1,[[0, 0,-0.5], [-0.2,0,0], [0,0,0.2], 
    `   [0.2,0,0.05], [-0.2, 0, 0.05], [0,0,0.2], [0.2, 0,0]])
        tutorial.execute_plan(cartesian_plan)

        input("============ Press `Enter` to transition to next letter")
        cartesian_plan, fraction = tutorial.plan_cartesian_path(1,[[-0.3, 0,0]]
        )
        tutorial.execute_plan(cartesian_plan)

        input("============ Press `Enter` to write the letter 'L' with a Cartesian path ...")
        cartesian_plan, fraction = tutorial.plan_cartesian_path(1,[[0,0,-0.5], [-0.2, 0,0]]
        )
        tutorial.execute_plan(cartesian_plan)

        input("============ Press `Enter` to transition to next letter")
        cartesian_plan, fraction = tutorial.plan_cartesian_path(1,[[-0.4, 0, 0.5]]
        )
        tutorial.execute_plan(cartesian_plan)

        input("============ Press `Enter` to write the letter 'S' with a Cartesian path ...")
        cartesian_plan, fraction = tutorial.plan_cartesian_path(1,[[0.2, 0, 0], [0, 0, -0.25], 
        [-0.2, 0, 0], [0, 0, -0.25],[0.2, 0, 0]])
        tutorial.execute_plan(cartesian_plan)

        print("============ Initial complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()


